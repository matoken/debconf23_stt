#!/bin/bash

while read str;
do
	if [ "${str}" == '' ]
	then
		echo ${str}
	elif [ "${str}" == "WEBVTT" ]
	then
		echo ${str}
	elif grep -q "^[0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9][0-9] --> [0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9][0-9]$" <<< "${str}"
	then
		echo ${str}
	elif grep -q "^ \[" <<< "${str}"
	then
		echo ${str}
	else
		echo -n ' '
		argos-translate -f en -t ja "${str}"
	fi
done < $1

