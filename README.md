# DebConf23 のビデオデータの文字起こし&機械翻訳

[](https://debconf23.debconf.org/static/img/jumbotron-image.a5a90d807f40.svg)
[DebConf 23](https://debconf23.debconf.org/)[1] の[ビデオアーカイブ](https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/)[2] をダウンロードして，Whisper.cpp[3] で文字起こしして，[Argos Translate](https://www.argosopentech.com/)[4] で日本語に機械翻訳してみたもの．

*.ja.vtt をビデオに埋め込んだり，ビデオと一緒に字幕ファイルとして読み込んで，DebConf の[Schedule](https://debconf23.debconf.org/schedule/)[5] から辿れるイベントページのetherpad やSlides を見ながら視聴すると良さそう．

[1] DebConf 23 https://debconf23.debconf.org/
[2] ビデオアーカイブ https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/
[3] Whisper.cpp https://github.com/ggerganov/whisper.cpp/
[4] Argos Translate https://www.argosopentech.com/
[5] https://debconf23.debconf.org/schedule/

## files

* *.vtt -> whisper.cpp(base.en)で文字起こしした字幕ファイル
* *.ja.vtt -> 文字起こし字幕ファイルをArgos Translate で機械翻訳した字幕ファイル

## ビデオデータの入手

https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/ から *.lq.webm を入手．ただし回線の問題で 06:44 で強制終了．

```
now=`date +%s` timeout=`date --date=$(date +%FT06:44:00) +%s` timeout $(( $timeout - $now )) wget -c -np -nd -nH -r -A .lq.webm https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/
```

## 動画を16k wav に変換

Whisper はそのままでいいが，Whisper.cpp は16k wev である必要があるので変換しておく．

```
$ ffmpeg -i ${FROM}.webm -ar 16000 -vn ${TO}.wav
```

## whisper.cpp で文字起こし

iGPU しかないのでWhisper.cpp で文字起こし．モデルには base.en を利用(tiny, base, small, medium, large とあるので余裕があればlarge で文字起こしし直したほうが精度は上がるはず)．txt と vtt 形式のファイルに書き出し．

```
$ ${WHISPERCPP}/main -m ${WHISPERCPP}/models/ggml-base.en.bin -l en --output-txt --output-vtt --print-colors -f ${WAV}
```

[![thumbnail](https://files.speakerdeck.com/presentations/e76503c6f3394f9b92d74af490e7df08/preview_slide_0.jpg)](https://speakerdeck.com/matoken/openainowhisper-deohurainwen-zi-qi-kosi-stt)  
[OpenAIのWhisperでオフライン文字起こし(STT)](https://gitlab.com/matoken/kagolug-2022.11/-/blob/master/slide/slide.adoc)

## Argos Translate で機械翻訳

使用は調べず.vtt の中を見てそれらしいことをしているだけなので間違っているかもしれない．
Argos Translate を1行毎にコマンドから呼んでいる．メモリに翻訳モデルデータを読み込んでいるはずなので本来より大分遅いはず．Pythonで書き直したり，LibreTranslate + LibreTranslate-sh を使うと速くなると思う．

```
$ cat ./trans.sh
#!/bin/bash

while read str;
do
        if [ "${str}" == '' ]
        then
                echo ${str}
        elif [ "${str}" == "WEBVTT" ]
        then
                echo ${str}
        elif grep -q "^[0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9][0-9] --> [0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9][0-9]$" <<< "${str}"
        then
                echo ${str}
        elif grep -q "^ \[" <<< "${str}"
        then
                echo ${str}
        else
                echo -n ' '
                argos-translate -f en -t ja "${str}"
        fi
done < $1

$ bash ./trans.sh ./${SUBTITLE}.vtt > ${SUBTITLE}.ja.vtt
```

[![](https://files.speakerdeck.com/presentations/92df0d8d7c014e42a43ed523c5d85da8/preview_slide_0.jpg)](https://speakerdeck.com/matoken/libretranslate)  
[機械翻訳をローカルマシンで( ArgosTranslate/LibreTranslate )](https://gitlab.com/matoken/kagolug-2022.12/-/blob/master/slide/slide.adoc)

## vttファイルの利用

`*.en.vtt` , `*.ja.vtt` が出来たのでこれを動画と同じディレクトリに置いて，vlc やmpv で動画を再生すると字幕が表示される．

![](vlc.jpg)

若しくは動画に字幕ファイルを埋め込む．

```
$ ffmpeg -i video.webm -vf subtitles=video.ja.vtt video_sub.webm
```

https://trac.ffmpeg.org/wiki/HowToBurnSubtitlesIntoVideo

