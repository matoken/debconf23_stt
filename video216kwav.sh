#!/bin/sh

for file in `\find . -name "*.webm"  -maxdepth 1 -type f`; do
	wav=`basename ${file} .webm`
	wav="${wav}.wav"
	if [ ! -e "${wav}" ]; then
		ffmpeg -i "${file}" -ar 16000 -vn "${wav}"
	fi
done

