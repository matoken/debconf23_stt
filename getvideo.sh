#!/bin/bash

get_video()
{
	now=`date +%s`
	timeout=${1}
	echo -n "$now - $timeout = "
	echo $(( $now - $timeout ))
	timeout $(( $timeout - ${now} )) wget -c -np -nd -nH -r -A .lq.webm -o - https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/
}

outoftime()
{
	echo 'out of time(23:15〜06:45)'
	exit
}

hour=`date +%H`
min=`date +%M`
if [ ${hour} -eq 23 ] && [ 15 -gt ${min} ]; then
	get_vide `date --date=$(date +%FT06:45:00 --date="tomorrow") +%s`
	exit

elif [ 6 -ge ${hour} ]; then
	if [ ${hour} -eq 6 ] && [ ${min} -gt 45 ]; then
		outoftime
		exit
	else
		get_video `date --date=$(date +%FT06:45:00) +%s`
		exit
	fi
fi

outoftime

