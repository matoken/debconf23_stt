#!/bin/sh

if !(which argos-translate 2>&1 >/dev/null); then
	echo "argos-translate: command not faund."
	exit;
fi

for envtt in `find . -name "*.en.vtt"  -maxdepth 1 -type f`; do
	javtt=`basename "${envtt}" .en.vtt`
	javtt="${javtt}.ja.vtt"
	if [ ! -e "${javtt}" ]; then
		./trans.sh "${envtt}" | tee "${javtt}"
	fi
done


