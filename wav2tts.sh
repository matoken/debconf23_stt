#!/bin/sh

for wav in `find . -name "*.wav"  -maxdepth 1 -type f`; do
	vtt=`basename "${wav}" .wav`
	vtt="${vtt}.en.vtt"
	echo "${vtt} ${wav}"
	if [ ! -e ${vtt} ]; then
		time ~/src/whisper.cpp/main -m ~/src/whisper.cpp/models/ggml-base.en.bin -l en --output-txt --output-vtt --print-colors -f "${wav}" 
		mv "${wav}.vtt" "${vtt}"
	fi
done

